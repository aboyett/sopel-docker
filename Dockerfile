FROM alpine:3.7

# create a user
RUN adduser -D -u 60001 sopel

# install sopel
RUN apk add --no-cache enchant gettext python3 && \
    pip3 install sopel

COPY run.sh /bin/run-sopel
COPY config /home/sopel/.sopel/

RUN chown -R sopel /home/sopel/

USER sopel

CMD ["/bin/run-sopel"]
