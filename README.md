# Dockerized Sopel IRC Bot

This is a quick packaging of the [Sopel IRC Bot](https://github.com/sopel-irc/sopel)
with a convenience wrapper to pull the configuration from environment variables.

## Quickstart
Copy `example.env` to your a location of your choice (`.env` in this example)

Run:
```
docker run --rm --env-file .env registry.gitlab.com/aboyett/sopel-docker/master:latest
```
