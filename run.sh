#!/bin/sh

echo "======== Processing config template"
envsubst < $HOME/.sopel/default.cfg.tmpl > $HOME/.sopel/default.cfg

echo "======== Runtime config"
cat $HOME/.sopel/default.cfg

if [ -f $HOME/.sopel/sopel-default.pid ] ; then
  echo "======== Existing PID file found. Removing..."
  rm $HOME/.sopel/sopel-default.pid
fi

echo "======== Launching bot"
exec sopel -c $HOME/.sopel/default.cfg
